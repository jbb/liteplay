{% extends "base" %}

{% block content %}
<h1>Privacy Policy</h1>

<h2>Definitions</h2>
<p>In the following text, client device refers to the device you use to visit this website.
Cookies refer to small pieces of information that websites can store on your client device.
The term "original url" refers to a video url that was entered into the text input box,
or a url linked underneath the video player as "original url".</p>

<h2>Cookies</h2>
<p>Liteplay itself never stores any cookies on your client device.
However, depending on the video url you enter on the index page,
pressing the watch button or otherwise submitting the form,
for example by pressing the enter key, can lead to third-party
cookies being stored on your client device,
since the video content is not hosted on our servers,
but by the service the video was published on.
For information on the cookies the video providers might use,
refer to the privacy policy of the individual providers.
You can find it by visiting the original url without using this service.</p>

<h2>Storage of information</h2>
<p>Liteplay stores no information about you.
Although for visiting this website you transfer information like your IP-Address and the browser you used to us,
this information is not stored or shared by us.</p>

<h2>Requests to third-party servers</h2>
<p>Since Liteplay does not provide video content itself, video files of urls you entered are
requested from remote servers of the respective provider.
Please refer to its privacy policy for more information about the provider,
by visiting the original url without using this service.</p>

{% endblock content %}
