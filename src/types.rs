// SPDX-FileCopyrightText: 2020 Jonah Brüchert <jbb@kaidan.im>
//
// SPDX-License-Identifier: AGPL-3.0-only

use youtube_dl::SingleVideo;

#[derive(Serialize, Default)]
pub struct Info {
    pub info: SingleVideo,
    pub url: String,
}

#[derive(Serialize)]
pub struct Version {
    pub youtube_dl: String,
    pub youtube_dl_api_server: String,
}
