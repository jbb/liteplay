// SPDX-FileCopyrightText: 2020 Jonah Brüchert <jbb@kaidan.im>
//
// SPDX-License-Identifier: AGPL-3.0-only

#[macro_use]
extern crate rocket;
#[macro_use]
extern crate serde_derive;

use rocket::fs::FileServer;
use rocket_dyn_templates::Template;

static PROJECT_VERSION: &'static str = env!("CARGO_PKG_VERSION");

mod api;
mod backend;
mod frontend;
mod host;
mod types;

#[launch]
fn rocket() -> _ {
    let api_routes = routes![
        api::info,
        api::play,
        api::version,
        api::thumbnail,
        api::search,
        api::playlist
    ];
    let frontent_routes = routes![
        frontend::index,
        frontend::url,
        frontend::video,
        frontend::api,
        frontend::stats,
        frontend::embed,
        frontend::privacy,
        frontend::search
    ];
    rocket::build()
        .mount("/api", api_routes)
        .mount("/", frontent_routes)
        .mount("/css", FileServer::from("css/"))
        .mount("/images", FileServer::from("images/"))
        .attach(Template::fairing())
}
