use rocket::async_trait;
use rocket::http::Status;
use rocket::outcome::Outcome;
use rocket::request::{self, FromRequest, Request};

// I can't really imagine this is not already implemented in rocket, but I wasn't able to find it so far.

pub struct Host(String);

#[derive(Debug)]
pub enum HostHeaderError {
    Missing,
    Unknown,
}

#[async_trait]
impl<'r> FromRequest<'r> for Host {
    type Error = HostHeaderError;

    async fn from_request(request: &'r Request<'_>) -> request::Outcome<Self, Self::Error> {
        //let protocol = request.raw_segment_str(0).unwrap_or(RawStr::from_str("http"));
        let hosts: Vec<_> = request.headers().get("Host").collect();
        match hosts.len() {
            0 => Outcome::Error((Status::BadRequest, HostHeaderError::Missing)),
            1 => Outcome::Success(Host(hosts[0].to_string())),
            _ => Outcome::Error((Status::BadRequest, HostHeaderError::Unknown)),
        }
    }
}

impl ToString for Host {
    fn to_string(&self) -> String {
        return self.0.clone();
    }
}
