// SPDX-FileCopyrightText: 2020 Jonah Brüchert <jbb@kaidan.im>
//
// SPDX-License-Identifier: AGPL-3.0-only

use std::ffi::CString;

use pyo3::prelude::{PyAnyMethods, Python};
use pyo3::types::{IntoPyDict, PyString};
use rocket::response::Redirect;
use rocket::serde::json::Json;
use youtube_dl::Playlist;

use crate::backend;
use crate::types::{Info, Version};

#[get("/info?<url>")]
pub async fn info(url: String) -> Json<Info> {
    let info = backend::extract_info(url.clone()).await.unwrap();
    return Json(Info {
        url: url.clone(),
        info,
    });
}

#[get("/playlist?<url>")]
pub async fn playlist(url: String) -> Json<Playlist> {
    return Json(backend::extract_playlist(url).await.unwrap());
}

#[get("/play?<url>")]
pub async fn play(url: String) -> Redirect {
    let info = &info(url).await.info;

    Redirect::to(backend::detect_video_source(info).unwrap())
}

#[get("/thumbnail?<url>")]
pub async fn thumbnail(url: String) -> Redirect {
    let info = &info(url).await.info;

    match info.thumbnail {
        Some(ref thumbnail) => Redirect::to(thumbnail.to_owned()),
        None => Redirect::to("invalid-thumbnail"), // TODO: Add fallback image
    }
}

#[get("/version")]
pub fn version() -> Json<Version> {
    let ytdl_version = Python::with_gil(|py| {
        let locals = [("ytdlversion", py.import("yt_dlp.version").unwrap())]
            .into_py_dict(py)
            .unwrap();
        py.eval(
            &CString::new("ytdlversion.__version__").unwrap(),
            None,
            Some(&locals),
        )
        .unwrap()
        .downcast::<PyString>()
        .unwrap()
        .extract()
        .unwrap()
    });

    return Json(Version {
        youtube_dl: ytdl_version,
        youtube_dl_api_server: crate::PROJECT_VERSION.to_owned(),
    });
}

#[get("/search?<query>&<number_of_results>")]
pub async fn search(query: String, number_of_results: usize) -> Json<Playlist> {
    Json(
        backend::extract_search(&query, number_of_results)
            .await
            .unwrap(),
    )
}
