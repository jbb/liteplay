use youtube_dl::{Playlist, SearchOptions, SingleVideo, YoutubeDl, YoutubeDlOutput};

pub async fn extract_info(url: String) -> anyhow::Result<SingleVideo> {
    Ok(YoutubeDl::new(&url)
        .socket_timeout("15")
        .flat_playlist(true)
        .format("best")
        .run_async()
        .await
        .map(YoutubeDlOutput::into_single_video)
        .map(Option::unwrap)?)
}

pub fn detect_video_source(info: &SingleVideo) -> Option<String> {
    match info.url {
        Some(ref url) => Some(url.to_owned()),
        None => match info.formats {
            Some(ref formats) => Some(formats.get(formats.len() - 1).unwrap().url.clone()).unwrap(),
            None => None,
        },
    }
}

pub async fn extract_playlist(url: String) -> anyhow::Result<Playlist> {
    Ok(YoutubeDl::new(&url)
        .flat_playlist(true)
        .run_async()
        .await
        .map(YoutubeDlOutput::into_playlist)
        .map(Option::unwrap)?)
}

pub async fn extract_search(query: &str, number_of_results: usize) -> anyhow::Result<Playlist> {
    Ok(
        YoutubeDl::search_for(&SearchOptions::youtube(query).with_count(number_of_results))
            .flat_playlist(true)
            .run_async()
            .await
            .map(YoutubeDlOutput::into_playlist)
            .map(Option::unwrap)?,
    )
}
