// SPDX-FileCopyrightText: 2020 Jonah Brüchert <jbb@kaidan.im>
//
// SPDX-License-Identifier: AGPL-3.0-only

use crate::backend;
use crate::host::Host;
use rocket::http::RawStr;
use rocket_dyn_templates::Template;
use std::collections::HashMap;
use youtube_dl::{Playlist, SingleVideo};

#[get("/?<url>")]
pub async fn video(url: String) -> Template {
    let offset = url.find("&list");
    let playlist: Option<Playlist>;
    let info: SingleVideo;

    match offset {
        // no playlist
        None => {
            playlist = None;
            info = backend::extract_info(url).await.unwrap();
        }
        // Playlist
        Some(o) => {
            let video_url = url.split_at(o).0.to_string();
            let playlist_url: String;

            // Workaround for non-complete youtube extractor urls
            if !url.starts_with("http") {
                playlist_url = format!("https://youtube.com/watch?v={}", url)
            } else {
                playlist_url = url;
            }

            info = backend::extract_info(video_url.clone()).await.unwrap();
            playlist = Some(backend::extract_playlist(playlist_url).await.unwrap())
        }
    }

    #[derive(Serialize)]
    struct Context {
        info: SingleVideo,
        page_title: Option<String>,
        video_source: String,
        playlist: Option<Playlist>,
    }

    let context = Context {
        page_title: info.title.to_owned(),
        video_source: backend::detect_video_source(&info).unwrap_or_default(),
        info,
        playlist,
    };

    Template::render("video", &context)
}

#[get("/search?<query>&<number_of_results>")]
pub async fn search(query: String, number_of_results: usize) -> Template {
    let search_results = backend::extract_search(&query, number_of_results).await;

    #[derive(Serialize)]
    struct Context {
        page_title: String,
        results: Playlist,
        number_of_results: usize,
        query: String,
    }

    let context = Context {
        page_title: format!("Search results for '{}'", &query),
        results: search_results.unwrap(),
        number_of_results,
        query,
    };

    Template::render("search", &context)
}

#[get("/")]
pub fn index() -> Template {
    #[derive(Serialize, Deserialize)]
    struct Context {
        page_title: String,
        enter_url: bool,
    }

    let context = Context {
        page_title: "Liteplay".to_owned(),
        enter_url: false,
    };

    Template::render("index", context)
}

#[get("/url")]
pub fn url() -> Template {
    #[derive(Serialize, Deserialize)]
    struct Context {
        page_title: String,
        enter_url: bool,
    }

    let context = Context {
        page_title: "Liteplay".to_owned(),
        enter_url: true,
    };

    Template::render("index", context)
}

#[get("/api")]
pub fn api() -> Template {
    let mut context = HashMap::<String, String>::new();
    context.insert("page_title".to_owned(), "API Documentation".to_owned());
    Template::render("api", context)
}

#[get("/info?<url>")]
pub async fn stats(url: String) -> Template {
    #[derive(Serialize)]
    struct Context {
        info: SingleVideo,
        page_title: String,
    }

    let context = Context {
        info: backend::extract_info(url).await.unwrap(),
        page_title: "Statistics".to_owned(),
    };
    Template::render("info", &context)
}

#[get("/embed?<url>")]
pub fn embed(url: String, host: Host) -> Template {
    let mut context = HashMap::<String, String>::new();
    context.insert("page_title".to_owned(), "Embed".to_owned());

    let encoded_url = RawStr::new(&url).percent_encode();
    context.insert(
        "video_url".to_owned(),
        format!("http://{}/api/play?url={}", host.to_string(), encoded_url),
    );
    context.insert(
        "poster_url".to_owned(),
        format!(
            "http://{}/api/thumbnail?url={}",
            host.to_string(),
            encoded_url
        ),
    );

    Template::render("embed", context)
}

#[get("/privacy")]
pub fn privacy() -> Template {
    let mut context = HashMap::<String, String>::new();
    context.insert("page_title".to_owned(), "Privacy Policy".to_owned());

    Template::render("privacy", context)
}
