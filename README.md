# Liteplay

Liteplay is a server for youtube-dl. It provides a simple java-script free webinterface, and a json api.
It is written in rust using the rocket framework, and uses PyO3 to call the python code of youtube-dl.
Therefore it requires rust nightly, since PyO3 depends on it.

To build and run the server, simply use cargo.
```
cargo run --release
```
